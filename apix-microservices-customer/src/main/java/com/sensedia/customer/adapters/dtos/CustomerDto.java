package com.sensedia.customer.adapters.dtos;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class CustomerDto {

  private String id;

  private String firstName;

  private String lastName;

  private String document;

  @JsonSerialize(using = MonetarySerializer.class)
  @Digits(integer = Integer.MAX_VALUE, fraction = 2)
  @DecimalMin(value = "0.0", inclusive = false)
  private BigDecimal grossSalary;

  private LocalDateTime createdAt;

  private LocalDateTime updatedAt;
}
